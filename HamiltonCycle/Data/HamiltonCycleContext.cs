﻿using Microsoft.EntityFrameworkCore;
using HamiltonCycle.Models;

namespace HamiltonCycle.Data
{
    public class HamiltonCycleContext : DbContext
    {
        public HamiltonCycleContext(DbContextOptions<HamiltonCycleContext> options)
            : base(options) { }

        public DbSet<Input> Input { get; set; }
        public DbSet<Output> Output { get; set; }
        public DbSet<Setting> Setting { get; set; }
        public DbSet<Solver> Solver { get; set; }
    }
}