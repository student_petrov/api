﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HamiltonCycle.Models
{
    public class Solver
    {        
        public int Id { get; set; }
        public int Setting_id { get; set; }
        public int File_id { get; set; }   
    }
}
