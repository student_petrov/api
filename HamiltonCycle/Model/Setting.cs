﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HamiltonCycle.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Population { get; set; }
        public int Mutation_chance { get; set; }
        public int Working_group { get; set; }
        public int Iteration_limit { get; set; }
        public int Neighbour_cities { get; set; }
        public int Neighbour_chance { get; set; }
    }
}
