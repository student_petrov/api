﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HamiltonCycle.Models
{
    public class Output
    {        
        public int Id { get; set; }
        public int Task_id { get; set; }
        [Url]
        public string Url { get; set; }
   
    }
}
