﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HamiltonCycle.Models
{
    public class Input
    {        
        public int Id { get; set; }
        public string Name { get; set; }
        [Url]
        public string Url { get; set; }
   
    }
}
