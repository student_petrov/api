﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HamiltonCycle.Data;
using HamiltonCycle.Models;

namespace HamiltonCycle.Controllers
{
    [Route("cycle/[controller]")]
    [ApiController]
    public class OutputsController : ControllerBase
    {
        private readonly HamiltonCycleContext _context;

        public OutputsController(HamiltonCycleContext context)
        {
            _context = context;
        }

        // GET: api/Outputs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Output>>> GetOutput()
        {
            return await _context.Output.ToListAsync();
        }

        // GET: api/Outputs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Output>> GetOutput(int id)
        {
            var output = await _context.Output.FindAsync(id);

            if (output == null)
            {
                return NotFound();
            }

            return output;
        }

        // PUT: api/Outputs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutOutput(int id, Output output)
        //{
        //    if (id != output.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(output).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!OutputExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Outputs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Output>> PostOutput(Output output)
        {
            _context.Output.Add(output);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOutput", new { id = output.Id }, output);
        }

        // DELETE: api/Outputs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Output>> DeleteOutput(int id)
        {
            var output = await _context.Output.FindAsync(id);
            if (output == null)
            {
                return NotFound();
            }

            _context.Output.Remove(output);
            await _context.SaveChangesAsync();

            return output;
        }

        private bool OutputExists(int id)
        {
            return _context.Output.Any(e => e.Id == id);
        }
    }
}
