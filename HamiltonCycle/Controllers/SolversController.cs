﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HamiltonCycle.Data;
using HamiltonCycle.Models;

namespace HamiltonCycle.Controllers
{
    [Route("cycle/[controller]")]
    [ApiController]
    public class SolversController : ControllerBase
    {
        private readonly HamiltonCycleContext _context;

        public SolversController(HamiltonCycleContext context)
        {
            _context = context;
        }

        // GET: api/Solvers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Solver>>> GetSolver()
        {
            return await _context.Solver.ToListAsync();
        }

        // GET: api/Solvers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Solver>> GetSolver(int id)
        {
            var solver = await _context.Solver.FindAsync(id);

            if (solver == null)
            {
                return NotFound();
            }

            return solver;
        }

        // PUT: api/Solvers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutSolver(int id, Solver solver)
        //{
        //    if (id != solver.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(solver).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SolverExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Solvers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Solver>> PostSolver(Solver solver)
        {
            _context.Solver.Add(solver);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSolver", new { id = solver.Id }, solver);
        }

        // DELETE: api/Solvers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Solver>> DeleteSolver(int id)
        {
            var solver = await _context.Solver.FindAsync(id);
            if (solver == null)
            {
                return NotFound();
            }

            _context.Solver.Remove(solver);
            await _context.SaveChangesAsync();

            return solver;
        }

        private bool SolverExists(int id)
        {
            return _context.Solver.Any(e => e.Id == id);
        }
    }
}
