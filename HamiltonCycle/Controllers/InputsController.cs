﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HamiltonCycle.Data;
using HamiltonCycle.Models;

namespace HamiltonCycle.Controllers
{
    [Route("cycle/[controller]")]
    [ApiController]
    public class InputsController : ControllerBase
    {
        private readonly HamiltonCycleContext _context;

        public InputsController(HamiltonCycleContext context)
        {
            _context = context;
        }

        // GET: api/Inputs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Input>>> GetInput()
        {
            return await _context.Input.ToListAsync();
        }

        // GET: api/Inputs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Input>> GetInput(int id)
        {
            var input = await _context.Input.FindAsync(id);

            if (input == null)
            {
                return NotFound();
            }

            return input;
        }

        //// PUT: api/Inputs/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutInput(int id, Input input)
        //{
        //    if (id != input.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(input).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!InputExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Inputs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Input>> PostInput(Input input)
        {
            _context.Input.Add(input);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInput", new { id = input.Id }, input);
        }

        // DELETE: api/Inputs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Input>> DeleteInput(int id)
        {
            var input = await _context.Input.FindAsync(id);
            if (input == null)
            {
                return NotFound();
            }

            _context.Input.Remove(input);
            await _context.SaveChangesAsync();

            return input;
        }

        private bool InputExists(int id)
        {
            return _context.Input.Any(e => e.Id == id);
        }
    }
}
